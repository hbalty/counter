<!doctype html>

<?php
require('conf.php');

// I WAS REALLY DRUNK WRITING THIS CODE ! PLEASE DON'T JUDGE ME
// BALTI

function get_ip() {
    // IP si internet partagé
    if (isset($_SERVER['HTTP_CLIENT_IP'])) {
        return $_SERVER['HTTP_CLIENT_IP'];
    }
    // IP derrière un proxy
    elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    // Sinon : IP normale
    else {
        return (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');
    }

}
$ip = get_ip();
if (!$ip){
    echo 'You dont\'t have an ip address !!! You are a magicien';
    die ;
}

function fetch_ip($ip,$pdo){
    $fetch_sth = $pdo->prepare('SELECT * FROM refresh WHERE ipaddress = :address');
    $fetch_sth->bindParam(':address',$ip);
    $fetch_sth->execute();
    $resultat = $fetch_sth->fetch(PDO::FETCH_ASSOC);
    return $resultat;
}


function increment($result, $pdo){
    $ip = $result['ipaddress'];
    $increment_sth = $pdo->prepare('UPDATE refresh SET refreshes = refreshes + 1 WHERE ipaddress = :ipaddress');
    $increment_sth->bindParam(':ipaddress',$ip);
    $x =$increment_sth->execute();
}

$resultat = fetch_ip($ip,$pdo);

if (!$resultat){
    $insert_sth = $pdo->prepare('INSERT INTO refresh(ipaddress,refreshes) values (:ipaddress,1)');
    $insert_sth->bindparam(":ipaddress",$ip);
    $x = $insert_sth->execute();
}


if (isset($_POST['increment'])){
    if (fetch_ip($ip,$pdo)){
        increment(fetch_ip($ip,$pdo),$pdo);
    } 
}

?>

<html> 
<head>
    <title>
        Incrementor
    </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
</head>
<body class="text-center">
<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
    <main role="main" class="inner cover">
        <h1 class="cover-heading"><?php echo fetch_ip($ip,$pdo)['refreshes']; ?></h1>
        <p class="lead"> Your IP address <?php echo $ip ; ?> refreshed your page <?php echo fetch_ip($ip,$pdo)['refreshes']; ?> times </p>
        <p class="lead">
            <form method="post"> <input type="submit" name="increment" class="btn btn-lg btn-secondary" value="Increment one more than 100 times to get a gift ! "/> </form>
        </p>
        <?php if (fetch_ip($ip,$pdo)['refreshes'] >= 100) echo "<img src='/img/gift.jpg'>" ; ?>
    </main>
</div>
</body>
</html>




